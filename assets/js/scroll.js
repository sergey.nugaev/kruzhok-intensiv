var app = window.STORE;

// КНОПКА ПОДРОБНЕЕ
// КНОПКА ПОДРОБНЕЕ
// КНОПКА ПОДРОБНЕЕ

// кнопка подробнее внизу заглавного экрана
var moreButton = document.querySelector('.Intro-line');

if (!$(moreButton).hasClass('isActive')) {
    if (window.scrollY < 10) {
        $(moreButton).addClass('isActive')
    }
}

// клик по кнопке подробнее
moreButton.addEventListener('click', function () {
    $('html, body').animate({
        scrollTop: $(window).height() - 60
    }, 1000);
});


// КАНВАС
// КАНВАС
// КАНВАС
var canvas = document.getElementById('circlesHover'),
    // прозрачный или непрозрачный
    canvasStatus = 1;


// СЕКЦИЯ КУРСА
// СЕКЦИЯ КУРСА
// СЕКЦИЯ КУРСА
var courseSection = document.querySelector('.Section.Course'),
    courseTop = $(courseSection).offset().top;

// СЕКЦИЯ ПАРТНЕРОВ
// СЕКЦИЯ ПАРТНЕРОВ
// СЕКЦИЯ ПАРТНЕРОВ
var partnerSection = document.querySelector('.Section.Partners'),
    partnerTop = $(partnerSection).offset().top;


window.addEventListener('scroll', function (event) {
    var scrollTop = window.scrollY,
        height = $(window).height(),
        headerStyle = document.querySelector('.Header').style,
        settings = app.settings;

    if (scrollTop < 10) {
        $(moreButton).addClass('isActive');
    } else {
        $(moreButton).removeClass('isActive');
    }

    if (scrollTop > (height - 61) && !settings.isHeaderVisible) {
      headerStyle.display = 'flex';
      setTimeout(() => { headerStyle.opacity = 1; }, 0);
      settings.isHeaderVisible = true;
    } else if (scrollTop <= (height - 61) && settings.isHeaderVisible) {
      headerStyle.opacity = 0;
      // setTimeout(() => { headerStyle.display = 'none'; }, 400); // this may cause a bug with fast scroll
      headerStyle.display = 'none';
      settings.isHeaderVisible = false;
    }
});
