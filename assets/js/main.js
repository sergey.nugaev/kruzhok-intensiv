// global store object
window.STORE = Object.assign({}, window.STORE);
window.STORE.price = 15000;

// массив всех аламни
// const alumniFeedback = [
//     {
//         id: 'nail',
//         name: 'Наиль',
//         age: 15,
//         isActive: 1,
//         imageUrl: 'img/alumnies/nail.jpg',
//         previewUrl: 'img/alumnies/nail-mini.jpg',
//         textHTML: 'Нам в школе такого не преподают, нет такого. Информатика есть, но там просто рисуем и фигуры разные делаем. Но это совсем не похоже на то, что мы делали здесь.'
//     },
//     {
//         id: 'pasha',
//         name: 'Паша',
//         age: 15,
//         isActive: 0,
//         imageUrl: 'img/alumnies/pasha.jpg',
//         previewUrl: 'img/alumnies/pasha-mini.jpg',
//         textHTML: 'Короче ребята приходите тут круто.Шуточки есть топовые.Интересно,ЕСТЬ ЕДА.И интересные задания .Приходите ...А неееее стоп забыл СУПЕР КРУТЫЕ ПРЕПОДАВАТЕЛИ. ЖДЁМ. 😂😉😋😜😁😊 (орфография сохранена — прим. редактора)'
//     },
//     {
//         id: 'kolya',
//         name: 'Коля',
//         age: 14,
//         isActive: 0,
//         imageUrl: 'img/alumnies/nikola.jpg',
//         previewUrl: 'img/alumnies/kolya-mini.jpg',
//         textHTML: 'На кружке о программировании на базе HTML и CSS, ну, буквально за 5 дней узнал большую часть базы. Очень понравилась задумка преподавателей и абсолютно почти все, что связано с кружком. Я очень доволен, что не пропустил такую возможность познакомиться со всеми заинтересовавшимися этим кружком и преподавателями, потому что некоторые могут о таком только мечтать! #vyazma_kruzhok'
//     },
//     {
//         id: 'veronika',
//         name: 'Вероника',
//         age: 13,
//         isActive: 0,
//         imageUrl: 'img/alumnies/veronika.jpg',
//         previewUrl: 'img/alumnies/veronika-mini.jpg',
//         textHTML: '"Кружок" — это не просто обычные курсы по программированию. Это весёлые, интересные и увлекательные курсы, где учат новому и полезному. Классные преподаватели, которые доступно рассказывают новые темы и помогут, если есть затруднения. Также занимательные задания и весёлые перерывы между занятиями.'
//     },
//     {
//         id: 'zhenya',
//         name: 'Женя',
//         age: 16,
//         isActive: 0,
//         imageUrl: 'img/alumnies/zhenya.jpg',
//         previewUrl: 'img/alumnies/zhenya-mini.jpg',
//         textHTML: 'Ребята из "Кружка" очень интересно преподают материал. Я никогда так не рвался на занятия, связанные с учёбой. Очень отзывчивые, готовые помочь и объяснить что-то любому ученику.'
//     }
// ];

const Projects = [
    {
        id: 'intensive',
        name: 'Кружок &mdash; интенсив',
        description: 'Интенсивы для городов-миллионников. Наши лучшие наработки в эффективном формате.',
        link: 'https://intensiv.kruzhok.io',
        anchor: 'вы уже здесь!',
        additionalText: '',
        imageUrl: 'img/projects/intensiv_1.jpg'
    },
    {
        id: 'tour',
        name: 'Кружок &mdash; тур',
        description: 'Всероссийский региональный проект. Посещаем 10 регионов в учебном году 17/18 с целью проведения серии занятий для школьников и учителей.',
        link: 'https://russia.kruzhok.io',
        anchor: 'russia.kruzhok.io',
        additionalText: '',
        imageUrl: 'img/projects/tour_1.jpg'
    },
    {
        id: 'constructor',
        name: 'Кружок &mdash; набор',
        description: 'Наборы для самостоятельного обучения. Высылаются по почте вместе с учебными материалами.',
        link: '#',
        anchor: 'старт — летом 2018',
        additionalText: '',
        imageUrl: 'img/projects/nabor_1.jpg'
    }
];

window.onload = function () {
    /*FEEDBACK COMPONENT*/
    // нагенерим блоков под всех аламни
    // const alamniListContainer = document.querySelector('.Feedback-alumniList');
    // alumniFeedback.forEach((alumni) => {
    //     let Div = document.createElement('div');
    //     Div.className += "Feedback-alumniContainer";
    //     //железобетонный инсерт
    //     let insertedHTML = '<div class="Feedback-alumniImage" style="background-image: url(' + alumni.previewUrl + ')" id="' + alumni.id + '"></div>';
    //     Div.innerHTML = insertedHTML;
    //     alamniListContainer.appendChild(Div);
    // });

    // генерим ширину в зависимости от того, сколько аламни в массиве
    // let allAlamniPreviews = document.querySelectorAll('.Feedback-alumniContainer');
    // allAlamniPreviews.forEach((preview) => {
    //     preview.style.width = 'calc((100% - 120px)/' + alumniFeedback.length + ')';
    // });

    // стейт текущего аламни
    // let alumniState = '';
    // // элементы, куда будем вставлять данные в зависимости от стейта
    // const alumniTextColumn = document.querySelector('.Feedback-descriptionColumn.right');
    // const alumniName = document.getElementById('name');
    // const alumniAge = document.getElementById('age');
    // const alumniBigPic = document.getElementById('alumniBigPic');
    // const alumniFeedbackText = document.getElementById('alumniFeedback');
    // //подставляем значения из объекта в зависимости от стейта
    // let setStateContent = (opacity) => alumniFeedback.forEach(function (alumni) {
    //     if (alumni.id === alumniState) {
    //
    //
    //         alumniTextColumn.style.opacity = opacity;
    //         alumniBigPic.style.opacity = opacity;
    //
    //         setTimeout(function () {
    //             alumniBigPic.style.backgroundImage = 'url(' + alumni.imageUrl + ')';
    //             alumniName.innerHTML = alumni.name;
    //             alumniAge.innerHTML = alumni.age;
    //             alumniBigPic.style.opacity = 1;
    //             alumniTextColumn.style.opacity = 1;
    //         }, 300);
    //         alumniFeedbackText.innerHTML = alumni.textHTML;
    //     }
    // });
    // setStateContent(1);
    // // делаю кликер
    // const clickImages = document.querySelectorAll('.Feedback-alumniImage');
    //
    // // функция приведения имени класса к дефолту
    // let clearClass = () => {
    //     clickImages.forEach((item) => {
    //         item.className = 'Feedback-alumniImage';
    //     })
    // }
    //
    //
    // clickImages.forEach((img) => {
    //     img.onclick = function () {
    //         alumniState = this.id;
    //         setStateContent(0);
    //         clearClass();
    //         this.className += ' isActive';
    //     }
    // });

    /*PROJECTS COMPONENT*/
    //нагенерим блоков под все проекты
    // const projectListContainer = document.querySelector('.Projects-container');
    // Projects.forEach((project) => {
    //     let ProjectDiv = 0;

    // if (project.id === 'tour') {
    //     ProjectDiv = document.createElement('a');
    //     ProjectDiv.href = 'https://russia.kruzhok.io';
    //     ProjectDiv.target = '_blank';
    // } else {
    // ProjectDiv = document.createElement('div');
    // }

    // ProjectDiv.className += 'Projects-item';
    // let ProjectHTML = '<div class="Projects-itemColumn"><h4 class="Projects-itemTitle">' + project.name + '</h4><p class="Projects-itemDescription">' + project.description + '</p><div class="Projects-linkBlock"><a href="' + project.link + '" class="Projects-link" target="_blank">' + project.anchor + '</a><span class="Projects-additionalFraze">' + project.additionalText + '</span></div></div><div class="Projects-itemImageBox"><div class="Projects-itemImage" style="background-image: url(' + project.imageUrl + ');"></div></div>';
    // ProjectDiv.innerHTML = ProjectHTML;
    // projectListContainer.appendChild(ProjectDiv);
    // });

    (function ($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0] = 'EMAIL';
        ftypes[0] = 'email';
        /*
                        * Translated default messages for the $ validation plugin.
                        * Locale: RU
                        */
        $.extend($.validator.messages, {
            required: "Это поле необходимо заполнить.",
            remote: "Пожалуйста, введите правильное значение.",
            email: "Пожалуйста, введите корректный адрес электронной почты.",
            url: "Пожалуйста, введите корректный URL.",
            date: "Пожалуйста, введите корректную дату.",
            dateISO: "Пожалуйста, введите корректную дату в формате ISO.",
            number: "Пожалуйста, введите число.",
            digits: "Пожалуйста, вводите только цифры.",
            creditcard: "Пожалуйста, введите правильный номер кредитной карты.",
            equalTo: "Пожалуйста, введите такое же значение ещё раз.",
            accept: "Пожалуйста, выберите файл с правильным расширением.",
            maxlength: $.validator.format("Пожалуйста, введите не больше {0} символов."),
            minlength: $.validator.format("Пожалуйста, введите не меньше {0} символов."),
            rangelength: $.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."),
            range: $.validator.format("Пожалуйста, введите число от {0} до {1}."),
            max: $.validator.format("Пожалуйста, введите число, меньшее или равное {0}."),
            min: $.validator.format("Пожалуйста, введите число, большее или равное {0}.")
        });
    }(jQuery));
    var $mcj = jQuery.noConflict(true);

    var popup = document.querySelector('.Signup');

    function openSignupPopup() {
        $('html, body').css({
            overflow: 'hidden',
            position: 'relative'
        });
        $('.Signup').css({
            '-webkit-overflow-scrolling': 'touch',
        });
        popup.className += ' isOpen';
    }

    function closeSignupPopup() {
        $('html, body').removeAttr('style');
        $('.Signup').css({
            '-webkit-overflow-scrolling': '',
        });
        popup.className = 'Signup';
    }

    var signUp = document.querySelectorAll('.openPopUp');
    signUp.forEach((item) => {
        item.addEventListener('click', function () {
            openSignupPopup();
        });
    });

    var closeSignUp = document.querySelector('.Signup-close');
    closeSignUp.addEventListener('click', function () {
        closeSignupPopup();
    });
};
