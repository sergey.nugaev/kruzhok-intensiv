var app = window.STORE;

!function () {

    "use strict";

    // ge1doot - last modified: 20150516
    var ge1doot = ge1doot || {};
    ge1doot.canvas = function (id) {
        "use strict";
        var canvas = {width: 0, height: 0, left: 0, top: 0, ctx: null, elem: null};
        var elem = document.getElementById(id);
        if (elem) {
            canvas.elem = elem;
        } else {
            canvas.elem = document.createElement("canvas");
            document.body.appendChild(canvas.elem);
        }
        canvas.elem.onselectstart = function () {
            return false;
        }
        canvas.elem.ondragstart = function () {
            return false;
        }
        canvas.ctx = canvas.elem.getContext("2d");
        canvas.setSize = function () {
            var o = this.elem;
            var w = this.elem.offsetWidth * 1;
            var h = this.elem.offsetHeight * 1;
            if (w != this.width || h != this.height) {
                for (this.left = 0, this.top = 0; o != null; o = o.offsetParent) {
                    this.left += o.offsetLeft;
                    this.top += o.offsetTop;
                }
                this.width = this.elem.width = w;
                this.height = this.elem.height = h;
                screen.height
                this.resize && this.resize();
            }
        }
        window.addEventListener('resize', canvas.setSize.bind(canvas), false);
        canvas.setSize();
        canvas.pointer = {
            x: 0,
            y: 0,
            dx: 0,
            dy: 0,
            startX: 0,
            startY: 0,
            canvas: canvas,
            touchMode: false,
            isDown: false,
            center: function (s) {
                this.dx *= s;
                this.dy *= s;
                endX = endY = 0;
            },
            sweeping: false,
            scale: 0
        }
        var started = false, endX = 0, endY = 0, scaling = false, pinchStart0X = 0, pinchStart1X = 0, pinchStart0Y = 0,
            pinchStart1Y = 0;
        var addEvent = function (elem, e, fn) {
            for (var i = 0, events = e.split(','); i < events.length; i++) {
                elem.addEventListener(events[i], fn.bind(canvas.pointer), false);
            }
        }
        var distance = function (dx, dy) {
            return Math.sqrt(dx * dx + dy * dy);
        }
        addEvent(window, "mousemove", function (e) {
            // e.preventDefault();
            this.touchMode = e.targetTouches;
            if (scaling && this.touchMode) {
                this.scale = distance(
                    this.touchMode[0].clientX - this.touchMode[1].clientX,
                    this.touchMode[0].clientY - this.touchMode[1].clientY
                ) / distance(
                    pinchStart0X - pinchStart1X,
                    pinchStart0Y - pinchStart1Y
                );
                if (this.pinch) this.pinch(e);
                return;
            }
            var pointer = this.touchMode ? this.touchMode[0] : e;
            this.x = pointer.clientX - this.canvas.left;
            this.y = pointer.clientY - this.canvas.top;
            if (started) {
                this.sweeping = true;
                this.dx = endX - (this.x - this.startX);
                this.dy = endY - (this.y - this.startY);
            }
            if (this.move) this.move(e);
        });
        return canvas;
    }


    // circle class
    var Circle = function (x, y, radius) {
        this.x = x;
        this.y = y;
        this.radius = 180;
        this.color = app.data.color;
        this.dragging = false;
        this.hovering = false;
        this.dx = 0;
        this.dy = 0;
    }

    /////////////////////////////////////////////////

    // main loop
    function run() {
        // request next frame
        requestAnimationFrame(run);
        // clear screen
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        // circles collision
        for (var i = 0, len = circles.length; i < len; i++) {
            var c0 = circles[i];
            c0.color = app.data.color;
            for (var j = i + 1; j < len; j++) {
                var c1 = circles[j];
                var dx = c1.x - c0.x;
                var dy = c1.y - c0.y;
                var r = c0.radius + c1.radius;
                var d = dx * dx + dy * dy;
                if (d < r * r) {
                    d = Math.sqrt(d);
                    dx /= d;
                    dy /= d;
                    d = (r - d) * 0.5;
                    dx *= d;
                    dy *= d;
                    if (!c0.dragging) {
                        c0.x -= dx;
                        c0.y -= dy;
                    }
                    if (!c1.dragging) {
                        c1.x += dx;
                        c1.y += dy;
                    }
                }
            }
        }

        // center attraction
        for (var i = 0; i < len; i++) {
            var c0 = circles[i];
            // var dx = c0.x - canvas.center.x;
            // var dy = c0.y - canvas.center.y;
            var dx = c0.x - pointer.x;
            var dy = c0.y - pointer.y;
            var f = 1 / c0.radius;
            c0.x -= dx * 0.2 * f;
            c0.y -= dy * 0.2 * f;
        }

        // draw circles
        for (var i = 0; i < len; i++) {
            var c0 = circles[i];
            ctx.beginPath();
            ctx.arc(c0.x, c0.y, c0.radius - 2, 0, 2 * Math.PI);
            ctx.fillStyle = c0.color;
            ctx.fill();
            // set dragging circle
            if (pointer.isDown && !dragging) {
                if (ctx.isPointInPath(pointer.x, pointer.y)) {
                    dragging = c0;
                    c0.dragging = true;
                    c0.dx = pointer.x - c0.x;
                    c0.dy = pointer.y - c0.y;
                }
            }

            if (!hovering) {
                if (ctx.isPointInPath(pointer.x, pointer.y)) {
                    dragging = c0;
                    c0.hovering = true;
                } else {
                    c0.hovering = false;
                    c0.dx = pointer.x - c0.x;
                    c0.dy = pointer.y - c0.y;
                }
            }
        }

        // drag circle
        if (dragging) {
            dragging.x = pointer.x - dragging.dx;
            dragging.y = pointer.y - dragging.dy;
        }

    }

    /////////////////////////////////////////////////

    // canvas
    var canvas = ge1doot.canvas("circlesHover");
    canvas.center = {x: 0, y: 0};
    var ctx = canvas.ctx;
    var pointer = canvas.pointer;
    var dragging = false;
    var hovering = false;

    // center
    canvas.resize = function () {
        this.center.x = this.width / 2;
        this.center.y = this.height / 2;
    }
    canvas.resize();

    // create circles
    var circles = [];
    for (var i = 0; i < 15; i++) {
        circles.push(
            new Circle(
                Math.random() * canvas.width,
                Math.random() * canvas.height,
                i < 50 ? Math.random() * 10 + 10 : Math.random() * 90 + 10
            )
        );
    }

    // pointer up
    pointer.up = function () {
        dragging.dragging = false;
        dragging = false;
    }

    // zyva!
    run();

}();
