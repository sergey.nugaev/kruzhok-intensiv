var gulp = require('gulp');
var del = require('del');
var path = require('path');
var _ = require('underscore/underscore');
var cloneDeep = require('lodash.clonedeep');
var runSequence = require('run-sequence').use(gulp);

var svgSprite = require('gulp-svg-sprite');

var webpack = require('webpack');
var gulpWebpack = require('./gulp.webpack.js');
var webpackConfig = require('./webpack.js');
var BundleTracker = require('webpack-bundle-tracker');

var concat = require('gulp-concat');
var server = require('gulp-server-livereload');
var surge = require('gulp-surge');

var webpackConfigs = {
    front: cloneDeep(webpackConfig),
    control: cloneDeep(webpackConfig)
};

var gzipConfig = {
    asset: "[path].gz[query]",
    algorithm: "gzip",
    test: /\.(js|css|svg|map)$/,
    threshold: 4096,
    minRatio: 0.8
};

gulp.task('setDevVars', function () {
    _.each(webpackConfigs, function (config) {
        config.devtool = 'cheap-module-source-map';
        config.watch = true;
        config.cache = true;
    });
});

gulp.task('setProductionVars', function () {
    _.each(webpackConfigs, function (config) {
        config.devtool = 'sourcemap';
        config.watch = false;
        config.cache = false;
        config.plugins.push(new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }));
    });
});

gulp.task('js', ['js-front']);

gulp.task('js-front', function () {
    var config = webpackConfigs.front;

    config.entry = './assets/js/app.js';
    config.output.path = path.join(__dirname, 'static');
    config.plugins.push(new BundleTracker({filename: './webpack.stats.json'}));

    gulpWebpack(webpackConfigs.front);
});

gulp.task('assets', function () {
    gulp.src('assets/libs/**/*')
        .pipe(gulp.dest('static/libs/'));
    gulp.src('assets/img/**/*')
        .pipe(gulp.dest('static/img'));
    gulp.src('assets/fonts/**/*')
        .pipe(gulp.dest('static/fonts'));
    gulp.src('assets/favicon/**/*')
        .pipe(gulp.dest('static/favicon'));
    gulp.src('assets/svg/**/*')
        .pipe(gulp.dest('static/svg'));
    gulp.src('index.html')
        .pipe(gulp.dest('static'));
});

gulp.task('svg', ['svg-front']);

gulp.task('svg-front', function () {
    return gulp.src('assets/svg/front/*.svg')
        .pipe(svgSprite({
            mode: {
                symbol: true
            }
        }))
        .pipe(gulp.dest('static/svg/front'));
});

gulp.task('watch', function () {
    gulp.watch('index.html', {interval: 1000}, ['assets']);
    gulp.watch('assets/css/libs/**/*', {interval: 1000}, ['assets']);
    gulp.watch('assets/js/libs/**/*', {interval: 1000}, ['assets']);
    gulp.watch('assets/img/**/*', {interval: 1000}, ['assets']);
    gulp.watch('assets/fonts/**/*', {interval: 1000}, ['assets']);
    gulp.watch('assets/svg/**/*', {interval: 1000}, ['svg']);
});

gulp.task('default', ['setDevVars', 'js', 'svg', 'assets', 'watch']);

// gulp.task('test', function (callback) {
//     runSequence(['setProductionVars', 'js', 'svg', 'assets'], callback);
// });

gulp.task('production', function (callback) {
    runSequence(['setProductionVars', 'js', 'svg', 'assets'], callback);
});

gulp.task('local', ['default'], function () {
    return gulp.src('./static')
        .pipe(server({
            host: '127.0.0.1',
            port: 1234,
            livereload: false,
            open: false
        }));
});

gulp.task('live', ['default'], function () {
    return gulp.src('./static')
        .pipe(server({
            host: '127.0.0.1',
            port: 1234,
            livereload: true,
            open: true
        }));
});

gulp.task('deploy', ['production'], function () {
    return surge({
        project: './static',
        domain: 'https://intensiv.kruzhok.io',
    });
});
