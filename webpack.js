var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var autoprefixer = require('autoprefixer');
var csswring = require('csswring');

module.exports = {
    output: {
        filename: 'bundle.js'
        // filename: '[name].[hash:8].js',
        // chunkFilename: '[chunkhash].js'
    },

    resolve: {
        modules: ['node_modules',
            path.join(__dirname, '/assets/'),]
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: ['node_modules', path.join(__dirname, '/assets/js')],
                enforce: 'pre',
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {cache: true}
                    }
                ]
            },
            {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: [
                        'css-loader',
                        'postcss-loader',
                        'less-loader',
                    ]
                })
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    fallbackLoader: 'style-loader',
                    loader: [
                        'css-loader',
                        'postcss-loader',
                    ]
                })
            },
            {
                test: /\.(jpg|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000'
            }
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery'
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                context: __dirname,
                postcss: [
                    autoprefixer({
                        browsers: ['last 2 version']
                    })
                ]
            }
        }),
        new ExtractTextPlugin({
            filename: 'style.css',
            disable: false,
            allChunks: true
        })
    ],

    externals: {
        jquery: 'jQuery'
    }
};
